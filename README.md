# AECmod_v300e
CRITICAL UPDATES: Localization.txt file fixed, which caused dialogue and note text not to display. Music has been removed for copyright claim-free streams. Thanks for the feedback, I'm listening - the sound blocks are still in the mod, but sounds.xml has been removed, therefore the music will no longer trigger.

AECmod [Atomic Energy Commission Mod] v3 is a total overhaul mod for 7 Days to Die that introduces story elements, NPC characters with dialog based objectives and quests, new POIs, and a beautiful, hand-crafted 12k map recreation of Warm Springs, Lake Moomaw, the Jackson River, Covington, and the beautiful forests and mountain ranges of Northwestern Virginia. Uncover the secrets of the Atomic Energy Commission in this work-in-progress mod project. Regular content and hotfix updates (see Discord for details).

AECmod is a great place for people who are looking for a familiar 7 Days to Die experience, enhanced by new themes, gameplay, and strategy elements.

Showcasing some of the most powerful mods available, AECmod uses SCore and XNPCCore to present new functionality, visceral situations, and daunting challenges. Vehicle Madness mod allows players waterway travel across this massive 12k map. Players can lockpick doors, trigger dynamic music, use blocks at hub POIs to spawn NPCs, then hire those NPC to guard, patrol, or bringing more firepower to the fight. To top it all off, Wookie Nookie's TheWinchester Mod grants players many quality of life improvements and immersion options, including enhanced zombie hordes, increased backpack size, and  Although there is a dark secret and rich story hidden beneath the mountains of Warm Springs, there is wild fun in a nomadic playthrough, where players makes no allegiances, no promises, and no guarantees to anyone other than themselves. Will you be a hero? Will you be a villain? Is there even a such thing in a post-World War 3 society? 


## IN THIS CURRENT BUILD, v300e
	- Single / Multiplayer Quests 
	- Handplaced Compopack Structures (if it wasn't handplaced, it isn't on the map yet)
	- D-LAMP Research Facility POI, spanning multiple kilometers in length.
	- The City of Convington
	- Unused TFP Yacht POIs
	- No proc gen areas on the map.
	  Everything thus far is hand-crafted for truly fun
	  (and terrifying) adventure mystery.

## FEATURES
Powered by SCore / XNPCCore!

THE WINCHESTER by Wookie Nookie
&
Vehicle Madness by Ragsy and ACone

Faction based NPC companions and enemies - soldiers, raiders, and civilians!

NPC packs by KhzMusik and Xyth

Lockpicking modlet by Sphereii and Xyth

Custom water animations by Cpt Krunch

Handcrafted 12k map by Goldmanvision and Cpt. Krunch, modelled after the real world mountain region of Warm Springs, Virginia, Lake Moomaw, and the city of Covington in North America. Southwestern desert region - "Sandia Peaks, New Mexico"

New custom POIs, including a multi-kilometer underground roadway, a massive underground Atomic Energy Commission facility called D-LAMP, Colonel Ackra's Outpost, and more.

Expanded NPC names - over 750 variations of names!

Highly curated experience: strictly hand-placed POIs (no procedural gen areas), game variable rebalancing, and more!


## THIRD PARTY MOD LIST
SCore
XNPCCore
Vehicle Madness
The Winchester Mod
Khzmusik Civilians
Xyth's Soldier NPC Pack
Xyth's Raiderz NPC Pack
Cpt Krunch's Water Animation Blocks


Special thanks to Krunch for his work on the Warm Springs map, as well as Guppycur, Bdubyah, Shavick, Sphereii and Xyth, and so many others that helped me along my 7DtD modding journey, and without their work in the community this dream would not be possible. Thanks y'all! 

This mod is in it's very early stages, but it has been play-tested for countless hours on dedicated servers, Steam multiplayer, and single player (all on a potato i3 Radeon RX550 computer).

## DISCORD
https://discord.com/channels/243577046616375297/965771485379706910

