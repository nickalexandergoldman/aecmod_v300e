  _  ___              _____
 | |/ (_)            / ____|
 | ' / _ _ __   __ _| |  __  ___ _ __
 |  < | | '_ \ / _` | | |_ |/ _ \ '_ \
 | . \| | | | | (_| | |__| |  __/ | | |
 |_|\_\_|_| |_|\__, |\_____|\___|_| |_|
                __/ |
               |___/
v0.13.1 - A19.6
7 Days to Die Random World Generator
Find out more at https://community.7daystodie.com/topic/23988-kinggen-a-random-world-generator-for-7-days-to-die/

SETTINGS:
border_biome=wasteland
border_size=medium
border_type=mountain
burnt_size=medium
check_output=false
cities_grid_size=medium
cities_level=40
cities_number=none
cities_size=medium
default_biome=pine forest
default_biome_tools=pine forest
desert_size=medium
heightmap=D:/SteamLibrary/steamapps/common/7 Days To Die/Mods/4a-GVmod_AECv300e/Worlds/AECmod-v300_Virginia_12k/dtm.raw
heightmap_smoothing=none
mountains_size=medium
multiple_generations=1
name=AECmod_v300_Virginia_12k-wip
output_folder=
png_heightmap=no
poi_mode=false
pois_list=D:/SteamLibrary/steamapps/common/7 Days To Die/Data/Prefabs/poislist.txt
pois_number=none
seed=Fwqdx8PI5p
size=12
skip_preview=false
snow_level=
snow_size=medium
spacing=medium
spawn_points=one
splat_file=D:/SteamLibrary/steamapps/common/7 Days To Die/Mods/4a-GVmod_AECv300e/Worlds/AECmod-v300_Virginia_12k/splat3.png
terrain_roughness=medium
towns_grid_size=medium
towns_number=none
towns_size=medium
traders_location=cities
traders_number=none
villages_grid_size=medium
villages_number=none
villages_size=medium
waste_size=medium
water_level=36
water_size=medium