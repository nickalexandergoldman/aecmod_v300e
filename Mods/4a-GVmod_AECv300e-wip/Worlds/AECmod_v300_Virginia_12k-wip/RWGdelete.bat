echo "about to delete RWG files... ARE YOU SURE?"
pause

del "splat3_processed.png"

del "splat4_processed.png"

del "dtm_processed.raw"

del "main.ttw"

del "main.ttw.bak"

del "checksums.txt"

echo "RWG files deleted!"
pause