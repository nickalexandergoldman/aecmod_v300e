@hubs
# A hub is a group of zones.
# ***** !!!!! SIZES INDICATE NUMBER OF INTERSECTIONS, NOT NUMBER OF POIS !!!!! *****
# ***** !!!!! DO NOT ABUSE MIN AND MAX, YOU WILL BREAK THE GENERATION !!!!! *****
# name,biomes,townships,zones,small_sizes,medium_sizes,large_sizes,prob,min,max
#    name: name of the hub
#    biomes: biomes where the hub can be generated
#    townships: townships of this hub
#    zones: zones contained by the hub
#    small_sizes: sizes the hub can have when users selects "small"
#    medium_sizes: sizes the hub can have when users selects "medium"
#    large_sizes: sizes the hub can have when users selects "large"
#    prob: probability of this hub being generated. A number between 0 and 1 is advisable, but it can be exceeded
#    min: minimum number of times this hub must be generated
#    max: maximum number of times this hub can be generated
default_city,burnt_forest/snow/pine_forest/desert/wasteland,city,residentialold/residentialnew/downtown/industrial/commercial,10/20,30/40/50,60/70/80,1,,
default_single_city,burnt_forest/snow/pine_forest/desert/wasteland,city,residentialold/residentialnew/downtown/industrial/commercial,80/100/120,130/150/170,180/200/220,1,,
default_town,burnt_forest/snow/pine_forest/desert/wasteland,town,residentialold/residentialnew/industrial/commercial,5/10,10/15/20,30/40,1,,
default_village,burnt_forest/snow/pine_forest/desert/wasteland,village,residentialold/residentialnew/commercial,1/2,1/2/3/4/5,6/8/10,1,,
oldwest_village,desert,village,oldwest,2/3,3/4,4/5,0.5,,

@zones
# A zone is a group of POIs.
# ***** !!!!! DO NOT REPEAT THE SAME COLOR MULTIPLE TIMES !!!!! *****
# name,red,green,blue
#    name: name of the zone
#    red: red value of rgb color
#    green: green value of rgb color
#    blue: blue value of rgb color
residentialold,255,100,0 #orange
residentialnew,255,255,0 #yellow
downtown,0,255,0 #green
industrial,0,0,255 #blue
commercial,255,0,255 #purple
oldwest,161,81,0 #brown

@pois
Air_Tunnel_7x100x5_(Krunch),7,5,100,0,1,burnt_forest/snow/pine_forest/desert/wasteland,rural/wilderness/town/city,residentialnew/residential,,
Air_Tunnel_7x10x5_(Krunch),10,7,5,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
Air_Tunnel_7x20x5_(Krunch),20,7,5,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
Air_Tunnel_7x30x5_(Krunch),30,7,5,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
Air_Tunnel_7x50x5_(Krunch),7,5,50,0,1,burnt_forest/snow/pine_forest/desert/wasteland,rural/wilderness/town/city,residentialnew/residential,,
Air_Tunnel_Door_7X5_(Krunch),13,5,7,0,2,burnt_forest/snow/pine_forest/desert/wasteland,rural/wilderness/town/city,residential,,
Asphalt_10x10_Krunch,10,1,10,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,,,
Asphalt_10x20_Krunch,10,1,20,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Asphalt_10x50_Krunch,10,1,50,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,,,
Asphalt_1x10_Krunch,1,1,10,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,,,
Bridge_End_02A_(Krunch),11,40,39,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
Bridge_Middle_02A_(Krunch),11,40,39,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
Bridge_Support_02A_(Krunch),3,30,23,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
Castle_Ravenloft_(KrunchEdit),112,151,136,-13,3,burnt_forest/snow/pine_forest/desert/wasteland,,any/residentialold/resdentialold,,
Castle_Ravenloft_2_(KrunchEdit),112,151,136,-13,3,burnt_forest/snow/pine_forest/desert/wasteland,,any/residentialold/resdentialold,,
Concrete_Walkway_20x100_Krunch,20,1,108,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Concrete_Walkway_20x20_Krunch,20,1,26,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Corner_Krunch,22,21,22,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,,,
Highway_Corner_T_Krunch,44,21,22,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Exit_Ramp_01_Krunch,14,14,18,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Exit_Ramp_02_Krunch,10,14,75,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Tile01_Extender_Krunch,10,10,1,-1,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Tile01_Krunch,14,14,9,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Tile_DeadEnd_Krunch,14,15,9,-1,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Tile_End_Krunch,14,13,49,-1,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Trader_Bob_01_Krunch,92,43,40,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Trader_Bob_02_Krunch,94,27,56,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Trader_Hugh_01_Krunch,94,34,41,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Trader_Hugh_02_Krunch,94,27,56,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Trader_Jen_01_Krunch,94,33,40,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Trader_Jen_02_Krunch,94,27,56,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Trader_Joel_01_Krunch,94,36,41,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Trader_Joel_02_Krunch,94,27,56,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Trader_Reckt_01_Krunch,94,35,41,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Highway_Trader_Rekt_02_Krunch,94,27,56,0,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Industrial_Fence_1x100_Krunch,100,3,1,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,,,
Ladder_tunnel_01_Krunch,11,10,10,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Ladder_tunnel_01_top_Krunch,11,3,10,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,,,
Ladder_tunnel_02_Krunch,3,10,3,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Ladder_tunnel_02_top_Krunch,5,13,5,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Lobby_Kells_Abbey(byKrunch),45,27,38,-5,0,burnt_forest/snow/pine_forest/desert/wasteland,,,,
Lobby_Santa_Muerte(byKrunch),45,32,39,-5,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Lobby_The_Lodge(byKrunch),46,28,38,-5,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Manhattan_Project_01_(Evilracc0on-Krunch),42,65,42,-5,2,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
MaunSell_Fort_by_MPLogue_Krunch,102,51,67,-15,0,desert/pine_forest/snow/burnt_forest/wasteland,wilderness,residentialold,,
Overlook_Hotel_by_Quagmire_Krunch,142,38,122,-7,2,snow,rural,residentialold,,
RWG_Mine_03_(Krunch),64,79,44,-55,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
Sidewalk_10x10_Krunch,10,1,10,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,,,
Sidewalk_10x20_Krunch,20,1,10,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Sidewalk_10x50_Krunch,10,1,50,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Sidewalk_1x10_Krunch,1,1,10,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,,,
Sidewalk_1x25_Krunch,25,1,1,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,,,
Sidewalk_1x50_Krunch,1,1,50,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,,,
Sky-Twintowers(by_TopMinder)_Krunch,150,174,150,-5,0,desert/pine_forest/snow,city,commercial,,
Skyscraper_05_Pepps_Tower_Krunch,42,80,42,-2,3,desert/pine_forest/snow/plains/burnt_forest/wasteland/forest,city,commercial,,
Spanish_Mission(The_Cure)_Krunch,80,25,144,-2,2,desert,rural/wilderness,nozone/none,,
WalkingDeadPrison_Laz_Man_SinderEdit_Krunch,212,55,200,-13,2,pine_forest,rural/wilderness/town/city,downtown/residential,,
WalkingDead_Woodbury_KrunchEdit,180,25,99,-2,3,burnt_forest/snow/pine_forest/desert/wasteland,rural/town/city,none,,
Wall_10x10x10_Krunch,10,10,10,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Wall_20x20x20_Krunch,20,20,20,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Wall_60x100_Krunch,1,60,100,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
Wall_60x10_Krunch,10,20,1,0,2,burnt_forest/snow/pine_forest/desert/wasteland,,none,,
mp_waste_bldg_02_grey_10story_(Krunch),42,57,43,-5,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
mp_waste_bldg_02_red_10story_(Krunch),42,57,43,-5,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
mp_waste_bldg_03_grey_10story_(Krunch),25,56,25,-4,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
mp_waste_bldg_03_red_10story_(Krunch),25,56,25,-4,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
mp_waste_bldg_04_grey_9story_(Krunch),42,52,42,-5,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
mp_waste_bldg_04_red_9story_(Krunch),42,52,42,-5,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
mp_waste_bldg_06_grey_8story_(Krunch),42,47,42,-5,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,
mp_waste_bldg_06_red_8story_(Krunch),42,47,42,-5,0,burnt_forest/snow/pine_forest/desert/wasteland,,residential,,